import { Component, ViewChild, OnInit} from '@angular/core';
import { Router} from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

import { Task} from '../../task.model';
import { TaskService} from '../../task.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  tasks: Task[];
  displayedColumns = ['id', 'description', 'trackedTime', 'bookingDate', 'actions'];
  dataSource: MatTableDataSource<Task>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private taskService: TaskService, private router: Router) {}

  /**
   * Set the paginator and sort after the view init since this component will
   * be able to query its view for the initialized paginator and sort.
   */
  ngOnInit() {
    this.getTasks();
    this.dataSource = new MatTableDataSource(this.tasks);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getTasks() {
    this.taskService
      .getTasks()
      .subscribe((data: Task[]) => {
        this.tasks = data;
        this.dataSource = new MatTableDataSource(this.tasks);
        console.log('Data requested ...');
        console.log(this.tasks);
      });
  }

  editTask(id) {
    this.router.navigate(['/tracker/${id}']);
  }

  deleteTask(id) {
    this.taskService.deleteTask(id).subscribe(() => {
      this.getTasks();
    });
  }
}
