import { Component, OnInit, OnDestroy } from '@angular/core';
import {TaskService} from '../../task.service';
import {Router} from '@angular/router';
import {Task} from '../../task.model';

@Component({
  selector: 'app-tracker',
  templateUrl: './tracker.component.html',
  styleUrls: ['./tracker.component.css']
})
export class TrackerComponent implements OnInit {
  private hours;
  private minutes;
  private seconds;
  counter: number;
  timerRef;
  running = false;
  startText = 'Start';

  constructor(private taskService: TaskService, private router: Router) { }

  ngOnInit() {
    this.hours = 0;
    this.minutes = 0;
    this.seconds = 0;
    this.counter = 0;
  }

  addTime(hh, mm, ss){
    this.hours = hh;
    this.minutes = mm;
    this.seconds = ss;
    const addedTime = this.hours * 60 * 60 * 1000 + this.minutes * 60 * 1000 + this.seconds * 1000 ;
    this.counter = this.counter + addedTime;
  }

  startTimer() {
    this.running = !this.running;
    if (this.running) {
      this.startText = 'Stop';
      const startTime = Date.now() - (this.counter || 0);
      this.timerRef = setInterval(() => {
        this.counter = Date.now() - startTime;
      });
    } else {
      this.startText = 'Resume';
      clearInterval(this.timerRef);
    }
  }

  clearTimer() {
    this.running = false;
    this.startText = 'Start';
    this.counter = undefined;
    clearInterval(this.timerRef);
  }

  ngOnDestroy() {
    clearInterval(this.timerRef);
  }

  book(description, trackedTime) {
    this.taskService.addTask(description, trackedTime, Date.now()).subscribe(() => {
      this.router.navigate(['/list']);
    });
  }
  cancel() {
    this.router.navigate(['/list']);
  }

}
