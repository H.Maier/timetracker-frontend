import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  uri = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getTasks() {
    return this.http.get(this.uri + '/api/task');
  }

  getTaskById(id) {
    return this.http.get(this.uri + '/api/task/' + id);
  }

  addTask(description, trackedTime, bookingDate) {
    const task = {
      description: description,
      trackedTime: trackedTime,
      bookingDate: bookingDate
    };
    return this.http.post(this.uri + '/api/task', task);
  }

  updateTask(id, description, trackedTime, bookingDate) {
    const task = {
      description: description,
      trackedTime: trackedTime,
      bookingDate: bookingDate
    };
    return this.http.patch(this.uri + '/api/task', task);
  }

  deleteTask(id) {
    return this.http.delete(this.uri + '/api/task/' + id);
  }
}
