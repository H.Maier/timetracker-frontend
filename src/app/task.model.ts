export interface Task {
  id: string;
  description: string;
  trackedTime: number;
  bookingDate: Date;
}
